#include <syslog.h>
#include "daemonize.h"

#include <fcntl.h>
#include <stdio.h>			// perror

int main(int argc, char* argv[])
{
	int i;

	daemonize("monomahd");

	for (i = 0; i < 10; i++) {
		syslog(LOG_DEBUG, "Прошло %d секунд", i);
		sleep(1);
	}
	syslog(LOG_INFO, "Демон остановлен");
	closelog();
	return 0;
}
