#include "daemonize.h"
#include <sys/stat.h>		// umask
#include <sys/types.h>		// umask
#include <sys/resource.h>	// rlimit
#include <signal.h>			// sigaction
#include <stdio.h>			// perror
#include <errno.h>			// perror
#include <unistd.h>			// setsid
#include <stdlib.h>			// exit
#include <syslog.h>			// syslog
#include <fcntl.h>			// open
#include <string.h>			// strerror

static const char* lockfilename = "/var/run/monomahd.pid";
static const int lockmode = (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

void daemonize(char* ident)
{
	int i, fd0, fd1, fd2;
	pid_t pid;
	struct rlimit rl;
	struct sigaction sa;

	if (getuid() != 0) {
		fprintf(stderr, "Невозможно запустить демона без прав root\n");
		exit(1);
	}

	openlog(ident, LOG_CONS, LOG_DAEMON);

	syslog(LOG_INFO, "Демон запускается...");

	umask(0);
	syslog(LOG_INFO, "Маска umask сброшена");

	if (getrlimit(RLIMIT_NOFILE, &rl) < 0) {
		syslog(LOG_EMERG, "Невозможно получить максимальный номер дескриптора");
		exit(1);
	}

	if ((pid = fork()) < 0) {
		syslog(LOG_EMERG, "Ошибка вызова fork()");
		exit(1);
	}
	else if (pid != 0)
		exit(0);
	setsid();
	syslog(LOG_INFO, "Демон стал лидером сессии, управляющий терминал утрачен");

	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction(SIGHUP, &sa, 0) < 0) {
		syslog(LOG_EMERG, "Невозможно установить игнорирование сигнала SIGHUP");
		exit(1);
	}
	if ((pid = fork()) < 0) {
		syslog(LOG_EMERG, "Ошибка вызова fork()");
		exit(1);
	}
	else if (pid != 0)
		exit(0);

	if (chdir("/") < 0) {
		syslog(LOG_EMERG, "Невозможно установить текущим каталогом /");
		exit(1);
	}
	syslog(LOG_INFO, "Текущий каталог установлен - /");

	if (rl.rlim_max == RLIM_INFINITY)
		rl.rlim_max = 1024;
	for (i = 0; i < rl.rlim_max; i++)
		close(i);

	fd0 = open("/dev/null", O_RDWR); // O_RDWR == read/write
	fd1 = dup(0);
	fd2 = dup(0);

	if (fd0 != 0 || fd1 != 1 || fd2 != 2) {
		syslog(LOG_EMERG, "Ошибочные файловые дескрипторы %d %d %d",
			fd0, fd1, fd2);
		exit(1);
	}
	syslog(LOG_INFO, "Дескрипторы %d, %d и %d перенаправлены на /dev/null",
		   fd0, fd1, fd2);

	if (isAlreadyRunning()) {
		syslog(LOG_EMERG, "Процесс прерван - демон уже запущен");
		exit(1);
	}

	syslog(LOG_INFO, "Демон запущен");
}

int isAlreadyRunning()
{
	int fd;
	char pid[16];

	fd = open(lockfilename, O_RDWR | O_CREAT, lockmode);
	if (fd < 0) {
		syslog(LOG_EMERG, "невозможно открыть %s : %s",
			lockfilename, strerror(errno));
		exit(1);
	}

	if (lockf(fd, F_TLOCK, 0) != 0) {
		if (errno == EACCES || errno == EAGAIN) {
			// Конфликт блокировки от другого процесса
			close(fd);
			return 1;
		}
		// невозможно установить блокировку по другой причине
		syslog(LOG_EMERG, "невозможно установить блокировку на %s: %s",
			lockfilename, strerror(errno));
		exit(1);
	}

	ftruncate(fd, 0);
	sprintf(pid, "%ld", (long)getpid());
	write(fd, pid, strlen(pid) + 1);
	return 0;
}
