#ifndef DAEMONIZE_H
#define DAEMONIZE_H
#include <unistd.h>			// setsid
#include <sys/stat.h>		// umask
#include <sys/types.h>		// umask
#include <sys/resource.h>	// rlimit
#include <signal.h>			// sigaction

void daemonize();
int isAlreadyRunning();

#endif
