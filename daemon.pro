#-------------------------------------------------
#
# Project created by QtCreator 2014-03-02T21:04:04
#
#-------------------------------------------------

QT       -= gui core qt

TARGET = daemon
CONFIG   -= console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    main.c \
    daemonize.c

HEADERS += \
    daemonize.h
